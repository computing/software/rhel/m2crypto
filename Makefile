#!/usr/bin/env make

package = m2crypto

.PHONY: sources
sources: $(package).spec
	spectool --get-files $<
